import { Injectable, HttpService, HttpException, HttpStatus } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

interface Song {
  id: number;
  name: string;
  link: string;
  albumCover: string;
  artist: string;
  duration: number;
}

export interface Data {
  data: Song[],
  total: number;
  prev?: string;
  next?: string;
}

@Injectable()
export class AppService {
  constructor(private http: HttpService){}

  fetch(query: { singer: string; index: string; }): Observable<Data> {
    let queryParams = `q=${query.singer}`;

    if (query.index) {
      queryParams = queryParams + `&index=${query.index}`;
    }

    return this.http.get(`https://api.deezer.com/search?${queryParams}`)
      .pipe(
        map((response): Data => {
          const songs = response.data;

          if (!songs.data.length) {
            throw new HttpException({message: 'No records found'}, HttpStatus.NOT_FOUND);
          }

          songs.data = songs.data.map((item): Song => ({
            id: item.id,
            name: item.title,
            link: item.link,
            albumCover: item.album.cover,
            artist: item.artist.name,
            duration: item.duration
          }));

          return songs;
        })
      );
  }
}
