import { Controller, Get, Query } from '@nestjs/common';
import { AppService } from './app.service';

@Controller('songs')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  fetchSongs(@Query() query: { singer: string; index: string; }) {
    return this.appService.fetch(query);
  }
}
